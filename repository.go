package main

import (
	"context"
	"errors"
	"strings"

	"github.com/mongodb/mongo-go-driver/mongo/options"

	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/bson/primitive"
	"github.com/mongodb/mongo-go-driver/mongo"

	pb "gitlab.com/lazybasterds/alpaca/node-service/proto/node"
)

const (
	dbName             = "alpacadb"
	nodeTypeCollection = "nodetype"
)

//Repository is an interface for getting information from the database.
type Repository interface {
	GetNodesPerFloor(ctx context.Context, floorID, nodeType, sortBy string) ([]*pb.Node, error)
	GetAllNodes(ctx context.Context, nodeType, sortBy string) ([]*pb.Node, error)
	GetNodeByID(ctx context.Context, nodeID string) (*pb.Node, error)
	Close(ctx context.Context)
}

//NodeRepository concrete implementation of the Repository interface.
type NodeRepository struct {
	db *DBConnection
}

// GetNodesPerFloor gets the nodes associated to the floor id provided
// If the nodeType is specified (e.g. Stores, Kiosk, etc.) then provide only the nodes
// for that specific type, otherwise return all nodes.
func (repo *NodeRepository) GetNodesPerFloor(ctx context.Context, floorID, nodeType, sortBy string) ([]*pb.Node, error) {
	//check first if there is a floorID provided
	if floorID == "" {
		return nil, errors.New("ERROR: GetNodesPerFloor - no floorID provided")
	}

	collection := repo.collection(nodeTypeCollection)

	filter := make(bson.M)
	filter["floorid"] = floorID
	if nodeType != "" {
		filter["type"] = nodeType
	}

	opts := &options.FindOptions{}
	sort := make(bson.M)

	if strings.EqualFold(sortBy, "category") {
		sort["details.category"] = 1
	}
	sort["name"] = 1 // Always sort alphabetically - even if the priority sort is by category. We sort those with same category alphabetically

	opts.Sort = sort

	cur, err := collection.Find(ctx, filter, opts)

	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var nodes []Node

	for cur.Next(ctx) {

		var node Node
		err := cur.Decode(&node)
		if err != nil {
			return nil, err
		}

		nodes = append(nodes, node)
	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	results := make([]*pb.Node, 0, len(nodes))
	for _, node := range nodes {
		results = append(results, &pb.Node{
			Id:   node.ID.Hex(),
			Name: node.Name,
			Type: node.Type,
			Coordinate: &pb.Coordinate{
				X: node.Coordinate.X,
				Y: node.Coordinate.Y,
				Z: node.Coordinate.Z,
			},
			FloorID: node.FloorID,
			Details: &pb.Details{
				Description: node.Details.Description,
				Logoid:      node.Details.LogoID.Hex(),
				Imageid:     node.Details.ImageID.Hex(),
				Location:    node.Details.Location,
				Category:    node.Details.Category,
				Tags:        node.Details.Tags,
				OpeningHours: &pb.OpeningHours{
					Sunday:    node.Details.OpeningHours.Sunday,
					Monday:    node.Details.OpeningHours.Monday,
					Tuesday:   node.Details.OpeningHours.Tuesday,
					Wednesday: node.Details.OpeningHours.Wednesday,
					Thursday:  node.Details.OpeningHours.Thursday,
					Friday:    node.Details.OpeningHours.Friday,
					Saturday:  node.Details.OpeningHours.Saturday,
				},
			},
			Neighbors: node.Neighbors,
		})
	}

	return results, nil
}

// GetAllNodes will get all the nodes in the MongoDB server
// If a type is specified, it will get that particular type,
// otherwise it will return all.
// This will return a slice of pb.Node for all the nodes, if an error
// is encountered it will return nil and the error
func (repo *NodeRepository) GetAllNodes(ctx context.Context, nodeType, sortBy string) ([]*pb.Node, error) {
	collection := repo.collection(nodeTypeCollection)

	filter := make(bson.M)
	if nodeType != "" {
		filter["type"] = nodeType
	}

	opts := &options.FindOptions{}
	sort := make(bson.M)

	if strings.EqualFold(sortBy, "category") {
		sort["details.category"] = 1
	}
	sort["name"] = 1 // Always sort alphabetically - even if the priority sort is by category. We sort those with same category alphabetically

	opts.Sort = sort

	cur, err := collection.Find(ctx, filter, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var nodes []Node

	for cur.Next(ctx) {
		var node Node
		err := cur.Decode(&node)
		if err != nil {
			return nil, err
		}

		nodes = append(nodes, node)

	}
	if err := cur.Err(); err != nil {
		return nil, err
	}

	var results = make([]*pb.Node, 0, len(nodes))

	//Convert model to proto object
	for _, node := range nodes {
		results = append(results, &pb.Node{
			Id:   node.ID.Hex(),
			Name: node.Name,
			Type: node.Type,
			Coordinate: &pb.Coordinate{
				X: node.Coordinate.X,
				Y: node.Coordinate.Y,
				Z: node.Coordinate.Z,
			},
			FloorID: node.FloorID,
			Details: &pb.Details{
				Description: node.Details.Description,
				Logoid:      node.Details.LogoID.Hex(),
				Imageid:     node.Details.ImageID.Hex(),
				Location:    node.Details.Location,
				Category:    node.Details.Category,
				Tags:        node.Details.Tags,
				OpeningHours: &pb.OpeningHours{
					Sunday:    node.Details.OpeningHours.Sunday,
					Monday:    node.Details.OpeningHours.Monday,
					Tuesday:   node.Details.OpeningHours.Tuesday,
					Wednesday: node.Details.OpeningHours.Wednesday,
					Thursday:  node.Details.OpeningHours.Thursday,
					Friday:    node.Details.OpeningHours.Friday,
					Saturday:  node.Details.OpeningHours.Saturday,
				},
			},
			Neighbors: node.Neighbors,
		})
	}

	return results, nil
}

// GetNodeByID will search the Database for Node specified by the nodeID provided.
// This will return the Node if the floor is found, otherwise it is nil
// If there are any error, it will return a nil together with the error.
func (repo *NodeRepository) GetNodeByID(ctx context.Context, nodeID string) (*pb.Node, error) {
	//check first if there is a floorID provided
	if nodeID == "" {
		return nil, errors.New("ERROR: GetNodeByID - no nodeID provided")
	}
	collection := repo.collection(nodeTypeCollection)

	objectID, err := primitive.ObjectIDFromHex(nodeID)
	if err != nil {
		return nil, err
	}

	var node Node
	err = collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&node)

	if err != nil {
		return nil, err
	}

	return &pb.Node{
		Id:   node.ID.Hex(),
		Name: node.Name,
		Type: node.Type,
		Coordinate: &pb.Coordinate{
			X: node.Coordinate.X,
			Y: node.Coordinate.Y,
			Z: node.Coordinate.Z,
		},
		FloorID: node.FloorID,
		Details: &pb.Details{
			Description: node.Details.Description,
			Logoid:      node.Details.LogoID.Hex(),
			Imageid:     node.Details.ImageID.Hex(),
			Location:    node.Details.Location,
			Category:    node.Details.Category,
			Tags:        node.Details.Tags,
			OpeningHours: &pb.OpeningHours{
				Sunday:    node.Details.OpeningHours.Sunday,
				Monday:    node.Details.OpeningHours.Monday,
				Tuesday:   node.Details.OpeningHours.Tuesday,
				Wednesday: node.Details.OpeningHours.Wednesday,
				Thursday:  node.Details.OpeningHours.Thursday,
				Friday:    node.Details.OpeningHours.Friday,
				Saturday:  node.Details.OpeningHours.Saturday,
			},
		},
		Neighbors: node.Neighbors,
	}, nil
}

//Close closes the session
func (repo *NodeRepository) Close(ctx context.Context) {
	// TODO: Revisit if this is the correct context when closing session
	repo.db.session.EndSession(ctx)
}

func (repo *NodeRepository) collection(coll string) *mongo.Collection {
	return repo.db.client.Database(dbName).Collection(coll)
}
